package main

import (
  "fmt"
  "sync"
)

// la mejor forma es con wait groups wg
func Hello( wg *sync.WaitGroup) {
  fmt.Println("Learning goroutines!")
  wg.Done()
}

func main() {
  var wg sync.WaitGroup
  wg.Add(1)
  go Hello(&wg)
  wg.Wait()
  fmt.Println("Mundo")
}
